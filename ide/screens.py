from browser import document

visible_screens = ["loading"]
default_loading_text = "Please wait..."

def show(screen, text=None):
    document[f"{screen}_screen"].hidden = False
    visible_screens.append(screen)
    if text and screen == "loading":
        document["loading_text"].set_text(text)

def hide(screen):
    document[f"{screen}_screen"].hidden = True
    visible_screens.remove(screen)
    if screen == "loading":
        document["loading_text"].set_text(default_loading_text)

def switch(screen, text=None):
    for other_screen in visible_screens:
        hide(other_screen)
    show(screen, text=text)
