from browser import document, window
from browser.html import TH, TR, B
from upload import uploader
import storage, theme, screens

class projectpicker_class:
    def __init__(self, project_loader, noeventinit=False):
        self.project_loader = project_loader
        self.noeventinit = noeventinit
        # Bind buttons
        if not noeventinit:
            document["project_new"].bind('click', self.create_project)
            document["export_all"].bind('click', self.export_all)
            document["import_all"].bind('click', self.import_all)
    
    def create_project(self, ev=None):
        # Ask for name
        name = window.prompt("What to name the new project?")
        # Abort if no name was given
        if not name or name == "":
            return
        # Create new project
        storage.set_project(name, {"filesystem": {'main.py': ''}})
        # Reload
        self.load_projects()
    
    def export_all(self, ev=None):
        # Ask for name
        name = window.alert("All data stored by the IDE will be downloaded. Please note that you will unlikely be able to use the downloaded data anywhere else than here! However, exporting projects as archive is planned. This backup contains all your projects and settings.")
        # Switch to loading screen
        screens.switch("loading")
        # Get all data from storage
        data = storage.get_all()
        # Define file format
        data["format"] = "ple"
        # Download all data from storage
        window.download(str(data), "pylabs-export.ple", "text/pylabs-export")
        # Return from loading screen screen again
        screens.switch("projectpicker")
    
    def import_all(self, ev=None):
        errinvalidformatmsg = "Import failed: Invalid format!"
        # Define importer
        def importer(data):
            # Verify action
            if not window.confirm("Warning: Continuing will replace all your projects and settings!"):
                return
            # Convert from string
            try:
                data = eval(data)
                # Check if data is in correct format
                if data["format"] != "ple":
                    window.alert(errinvalidformatmsg)
                    return
            except:
                window.alert(errinvalidformatmsg)
                return
            del data["format"]
            # Import from data
            storage.set_all(data)
            # Reload project list
            self.load_projects()
        # Run uploader
        uploader(importer)
    
    def open_project(self, ev=None, project=None, action="open"):
        if ev: # Get variables from event
            project = ev.target["project"]
            action = ev.target["action"]
        if action == "delete": # Delete action
            if window.confirm(f'"{project}" will be deleted PERMANENTLY! Are you sure to continue?'):
                storage.del_project(project)
        elif action == "rename": # Rename action
            newname = window.prompt(f'What to name "{project}"?')
            if newname and newname != "":
                storage.rename_project(project, newname)
        elif action == "open": # Open action
            self.project_loader(storage.get_project_fs(project), project, self.noeventinit)
            return
        # Reload
        self.load_projects()
    
    def load_projects(self):
        # Get project list
        projects = storage.get_project_list()
        # Clean up stuff
        document["project_list"].clear()
        # Create empty list if there are no projects
        if projects == []:
            document["project_list"].appendChild(TR(TH("There are no projects (yet). Create one to continue!")))
            return
        # Create list of projects
        for project in projects:
            # Create elements
            elem_text = B(project, style={"font-size": "23px"})
            elem_text["class"] = "noevents"
            elem_open = TH(elem_text, action="open", project=project)
            elem_rename = TH(theme.get_icon("edit"), action="rename", project=project, width="5%")
            elem_delete = TH(theme.get_icon("delete"), action="delete", project=project, width="5%")
            # Set class manually (thx for not letting me do that directly, python -.-)
            elem_open["class"] = "ui_cell"
            elem_rename["class"] = "ui_cell"
            elem_delete["class"] = "ui_cell"
            # Create TR element
            element = TR(style={"cursor": "pointer"})
            # Append TH elements
            element.appendChild(elem_open)
            element.appendChild(elem_rename)
            element.appendChild(elem_delete)
            # Set binding
            element.bind('click', self.open_project)
            # Append element
            document["project_list"].appendChild(element)
