from browser import document, window
from vfs_importer import importer_class
import sys, os, io
import tb as traceback


class term_in_class:
    def __getattr__(self, name):
        raise RuntimeError("Only read() and readline() are supported for stdin.")
    
    def read(self, data):
        return input() + '\n'
    
    def readline(self, data):
        return input() + '\n'


class term_out_class:
    def __init__(self, terminal):
        # Define basic stuff
        self.terminal = terminal
        # Create file descriptor
        self.fd = io.StringIO()
        # Replace write()
        self.fd._real_write = self.fd.write
        self.fd.write = self.write
    
    def write(self, data):
        self.terminal.value += data
        return self.fd._real_write(data)


class sandbox_class:
    def __init__(self, vfs):
        # Blacklist
        self.blacklist = ["browser", "webbrowser", "javascript", "posix", "importlib", "imp"]
        # Initalisation stuff
        self.vfs = vfs
        self.terminal = document["terminal"]
        self.importer = importer_class(vfs, blacklist=self.blacklist).__import__
        # Get terminal fd
        term_out = term_out_class(self.terminal)
        term_in = term_in_class()
        try:
            del sys.stdin
        except:
            pass
        # Initalize terminal output/input
        sys.stdin = term_in
        sys.stdout = term_out.fd
        sys.stderr = term_out.fd
        # Create namespace
        self.namespace = {"__import__": self.importer, "open": vfs.open}
    
    def run(self, path):
        vfs = self.vfs
        # Open the file
        with vfs.open(path) as f:
            script = f.read()
        # Run the code
        try:
            exec(script, self.namespace, self.namespace)
        except:
            traceback.print_exc()
