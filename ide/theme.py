from browser.html import IMG

def get_icon(icon):
    element = IMG(src=f"icons/{icon}.svg", style={"filter": "invert(22%) sepia(49%) saturate(4266%) hue-rotate(11deg) brightness(101%) contrast(105%)"})
    element["class"] = "noevents"
    return element
