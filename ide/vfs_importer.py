import sys, os#, window
from types import ModuleType

class importer_class:
    def __init__(self, vfs=None, blacklist=[], pypath=[".", "./Lib/site-packages"]):
        self.pypath = pypath
        self.blacklist = blacklist
        self.backup_importer = __import__
        if vfs:
            self.vfs = vfs
        else:
            self.vfs = os
    
    
    def __import__(self, name, globals=None, locals=None, fromlist=(), level=0):
        """Import a module.
    
        The 'globals' argument is used to infer where the import is occurring from
        to handle relative imports. The 'locals' argument is ignored. The
        'fromlist' argument specifies what should exist as attributes on the module
        being imported (e.g. ``from module import <fromlist>``).  The 'level'
        argument represents the package location to import from in a relative
        import (e.g. ``from ..pkg import mod`` would have a 'level' of 2).
    
        """
        try:
            if level == 0:
                module = self._gcd_import(name)
            else:
                globals_ = globals if globals is not None else {}
                package = self._calc___package__(globals_)
                module = self._gcd_import(name, package, level)
            if not fromlist:
                # Return up to the first dot in 'name'. This is complicated by the fact
                # that 'name' may be relative.
                if level == 0:
                    return self._gcd_import(name.partition('.')[0])
                elif not name:
                    return module
                else:
                    # Figure out where to slice the module's name up to the first dot
                    # in 'name'.
                    cut_off = len(name) - len(name.partition('.')[0])
                    # Slice end needs to be positive to alleviate need to special-case
                    # when ``'.' not in name``. 
                    return sys.modules[module.__name__[:len(module.__name__)-cut_off]]
            else:
                return self._handle_fromlist(module, fromlist, self._gcd_import)
        except ModuleNotFoundError:   # Alternative import
            if not name in self.blacklist:
                return self.backup_importer(name, globals, locals, fromlist, level)
            else:
                raise
    
    def _calc___package__(self, globals):
        """Calculate what __package__ should be.
    
        __package__ is not guaranteed to be defined or could be set to None
        to represent that its proper value is unknown.
    
        """
        package = globals.get('__package__')
        spec = globals.get('__spec__')
        if package is not None:
            if spec is not None and package != spec.parent:
               # window.console.log("Warn: __package__ != __spec__.parent "
               #     f"({package!r} != {spec.parent!r})",
               #     ImportWarning, stacklevel=3)
               pass
            return package
        elif spec is not None:
            return spec.parent
        else:
          #  window.console.log("Warn: can't resolve package from __spec__ or __package__, "
          #      "falling back on __name__ and __path__",
          #      ImportWarning, stacklevel=3)
            package = globals['__name__']
            if '__path__' not in globals:
                package = package.rpartition('.')[0]
        return package
    
    
    def _gcd_import(self, name, package=None, level=0):
        """Import and return the module based on its name, the package the call is
        being made from, and the level adjustment.
    
        This function represents the greatest common denominator of functionality
        between import_module and __import__. This includes setting __package__ if
        the loader did not.
    
        """
        #_sanity_check(name, package, level)
        if level > 0:
            name = self._resolve_name(name, package, level)
        return self._find_and_load(name)
    
    
    def _call_with_frames_removed(f, *args, **kwds):
        """remove_importlib_frames in import.c will always remove sequences
        of importlib frames that end with a call to this function
    
        Use it instead of a normal call in places where including the importlib
        frames introduces unwanted noise into the traceback (e.g. when executing
        module code)
        """
        return f(*args, **kwds)
    
    
    def _handle_fromlist(self, module, fromlist, import_, *, recursive=False):
        _NEEDS_LOADING = object()
        """Figure out what __import__ should return.
    
        The import_ parameter is a callable which takes the name of module to
        import. It is required to decouple the function from assuming importlib's
        import implementation is desired.
    
        """
        # The hell that is fromlist ...
        # If a package was imported, try to import stuff from fromlist.
        if hasattr(module, '__path__'):
            for x in fromlist:
                if not isinstance(x, str):
                    if recursive:
                        where = module.__name__ + '.__all__'
                    else:
                        where = "``from list''"
                    raise TypeError(f"Item in {where} must be str, "
                                    f"not {type(x).__name__}")
                elif x == '*':
                    if not recursive and hasattr(module, '__all__'):
                        self._handle_fromlist(module, module.__all__, import_,
                                        recursive=True)
                elif not hasattr(module, x):
                    from_name = '{}.{}'.format(module.__name__, x)
                    try:
                        _call_with_frames_removed(import_, from_name)
                    except ModuleNotFoundError as exc:
                        # Backwards-compatibility dictates we ignore failed
                        # imports triggered by fromlist for modules that don't
                        # exist.
                        if (exc.name == from_name and
                            sys.modules.get(from_name, _NEEDS_LOADING) is not None):
                            continue
                        raise
        return module
    
    
    def _find_and_load(self, name):
        # Get location of module
        modfound = False
        for path in self.pypath:
            for location in [f"{path}/{name}.py", f"{path}/{name}/__init__.py"]:
                try:
                    self.vfs.open(location).close()
                    modfound = True
                    break
                except FileNotFoundError:
                    pass
            if modfound:
                break
        if not modfound:
            raise ModuleNotFoundError(name)
        # Check if location was found
        if not location:
            raise ModuleNotFoundError(name)
        # Read the file
        with self.vfs.open(location) as file:
            modcode = file.read()
        # Load it
        mod = ModuleType(name)
        # Add module to environment
        sys.modules[name] = mod
        # Run the module
        exec(modcode, mod.__dict__)
        # Return module
        return mod
