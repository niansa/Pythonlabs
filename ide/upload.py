from browser import bind, html, window

def uploader(file_handler):
    # Create hidden upload button
    uploadbtn = html.INPUT(type="file")
    # Create file loader function
    @bind(uploadbtn, "change")
    def file_loader(ev):
        # Get file
        file = ev.target.files[0]
        # Create FileReader instance
        reader = window.FileReader.new()
        reader.readAsText(file)
        # Define file importer
        @bind(reader, "load")
        def run_file_handler(ev):
            file_handler(reader.result)
    # Click it
    uploadbtn.click()
