from browser.local_storage import storage
from browser.object_storage import ObjectStorage
from browser import window
object_storage = ObjectStorage(storage)

def init():
    if not "projects" in object_storage:
        object_storage["projects"] = {}
    if not "settings" in object_storage:
        object_storage["settings"] = {}


def get_all():
    data = {"projects": {}}
    data.update({"settings": object_storage["settings"]})
    for project in get_project_list():
        data["projects"][project] = object_storage["project_"+project]
    return data

def set_all(data, overwrite=True):
    if overwrite:
        for project in get_project_list():
            del_project(project)
        set_settings({})
    for key in data["projects"].keys():
        object_storage["project_"+key] = data["projects"][key]
    object_storage["settings"] = data["settings"]


def get_settings():
    return object_storage["settings"]

def set_settings(value):
    object_storage["settings"] = value

def set_setting(key, value):
    settings = object_storage["settings"]
    settings[key] = value
    object_storage["settings"] = settings


def get_project_list():
    projects = []
    for key in object_storage.keys():
        if key.startswith("project_"):
            projects.append(key[8:])
    return projects

def get_project(project, value):
    return object_storage["project_"+project]

def set_project(project, value):
    object_storage["project_"+project] = value

def del_project(project):
    del object_storage["project_"+project]

def set_project_fs(project, value):
    data = object_storage["project_"+project]
    data["filesystem"] = value
    object_storage["project_"+project] = data

def get_project_fs(project):
    return object_storage["project_"+project]["filesystem"]

def rename_project(project, value):
    data = object_storage["project_"+project]
    del object_storage["project_"+project]
    object_storage["project_"+value] = data
