from browser import document, window, bind
from javascript import JSON
from sandboxrun import sandbox_class
from upload import uploader
import storage, theme, screens
from browser.html import A, B, P, TR, TD, TH, IMG, BR, TABLE, CENTER, INPUT
import screens
import os, tb

class ui_class:
    def __init__(self, vfs, project_picker, project_name, noeventinit=False):
        self.vfs = vfs
        self.project_picker = project_picker
        self.project_name = project_name
        self.ui = document["ui_screen"]
        self.basetable = self.ui.innerHTML
        self.header_left = document["header_left"]
        self.header_right = document["header_right"]
        self.filemenu = document["filemenu"]
        self.codearea = document["codearea"]
        self.terminal = document["terminal"]
        self.terminal_cell = document["terminal_cell"]
        self.terminal_minimize_btn = document["terminal_minimizer"]
        self.codearea_header = document["codearea_header"]
        self.codearea_save_btn = document["codearea_save"]
        self.codearea_run_btn = document["codearea_run"]
        self.project_save_btn = document["project_save"]
        self.project_export_btn = document["project_export"]
        self.project_import_btn = document["project_import"]
        self.project_close_btn = document["project_close"]
        self.filemenu_one_up_btn = document["filemenu_one_up"]
        self.filemenu_new_file_btn = document["filemenu_new_file"]
        self.filemenu_new_folder_btn = document["filemenu_new_folder"]
        # Assign default values
        self.current_file = ""
        self.codearea_default = self.codearea.value
        self.terminal_default = self.terminal.value
        # Assign functions to buttons
        if not noeventinit:
            self.codearea_save_btn.bind("click", self.save_file)
            self.codearea_run_btn.bind("click", self.run_file)
            self.terminal_minimize_btn.bind("click", self.terminal_minimize)
            self.project_save_btn.bind("click", self.save_fs)
            self.project_export_btn.bind("click", self.export_project)
            self.project_import_btn.bind("click", self.import_project)
            self.project_close_btn.bind("click", self.close_project)
            self.filemenu_new_file_btn.bind("click", self.new_file)
            self.filemenu_new_folder_btn.bind("click", self.new_folder)
            self.filemenu_one_up_btn.bind("click", self.one_up)
        # Make sure save elements have default values
        self.codearea_save_btn.disabled = True
        self.codearea_run_btn.disabled = True
        self.filemenu_one_up_btn.disabled = True
        self.codearea.disabled = True
        self.terminal.disabled = True
        self.codearea.set_value(self.codearea_default)
        self.terminal.set_value(self.terminal_default)
        # Try to save the filesystem when the user leaves the page
        window.bind("beforeunload", self.save_fs)
        # Initalise sandbox
        self.sandbox = sandbox_class(vfs)
        # Define fs events
        vfs._def_update_event(self.update_path)
        self._enable_update_event = True

    def reload(self):
        # clear current user interface
        self.ui.clear()
        #create base table
        document["userinterface"].innerHTML = self.basetable
        self.__init__(self.vfs)
    
    def terminal_minimize(self, ev=None):
        other_style = self.terminal_cell["other_style"]
        self.terminal_cell["other_style"] = self.terminal_cell["style"]
        self.terminal_cell["style"] = other_style
    
    def save_file(self, ev=None):
        self._enable_update_event = False
        # Get codeareas content
        data = self.codearea.value
        # Remove old file
        self.vfs.remove(self.current_file)
        # Write new file
        with self.vfs.open(self.current_file, mode="w") as file:
            file.write(data)
        self._enable_update_event = True
    
    def save_fs(self, ev=None):
        # Switch to loading screen
        screens.switch("loading", "Saving project...")
        # Give the user a hint
        if ev and ev.target == self.project_save_btn:
            window.alert("Please note that saving the project manually is usualy not required as it is done automatically on certain actions.")
        # Save current file if any file is opened
        if self.current_file != "":
            self.save_file(ev)
        # Dump current filesystem to storage
        storage.set_project_fs(self.project_name, self.vfs.filesystem)
        # Switch back to ui screen
        screens.switch("ui")
    
    def run_file(self, ev=None):
        # First, save the file
        self.save_file()
        # Now clear and enable the terminal buffer
        self.terminal.value = ""
        self.terminal.disabled = False
        # Disable codearea
        self.codearea.disabled = True
        # Then run the script in a sandbox
        try:
            self.sandbox.run(self.current_file)
        except:
            tb.print_exc()
        # Reenable codearea
        self.codearea.disabled = False
    
    def new_file(self, ev=None):
        # Ask for name
        name = window.prompt("What to name the new file?")
        # Abort if no name was given
        if not name or name == "":
            return
        # Create a new file
        self.vfs.touch(name)
    
    def new_folder(self, ev=None):
        # Ask for name
        name = window.prompt("What to name the new folder?")
        # Abort if no name was given
        if not name or name == "":
            return
        # Create a new folder
        self.vfs.mkdir(name)
    
    def one_up(self, ev=None):
        self.path_open(path="..", entrytype="fs.directory")
    
    def export_project(self, ev=None):
        # Save current filesystem
        self.save_fs()
        # Get filesystem
        data = self.vfs.filesystem
        # Set file format
        data["format"] = "plp"
        # Download filesystem
        window.download(str(data), f"{self.project_name}.plp", "text/pylabs-project")
    
    def import_project(self, ev=None):
        errinvalidformatmsg = "Import failed: Invalid format!"
        # Define importer
        def importer(data):
            # Verify action
            if not window.confirm("Warning: Continuing will replace all files in current project!"):
                return
            # Convert from string
            try:
                data = eval(data)
                # Check if data is in correct format
                if data["format"] != "plp":
                    window.alert(errinvalidformatmsg)
                    return
            except:
                window.alert(errinvalidformatmsg)
                return
            del data["format"]
            # Save and close current file
            try:
                self.save_file()
                self.close_file()
            except:
                pass
            # Import from data
            self.vfs.filesystem = data
            # Reload filemanager
            self.reload_filemanager()
        # Run uploader
        uploader(importer)
    
    
    
    def close_file(self):
        # Set default values
        self.codearea.disabled = True
        self.codearea.set_value(self.codearea_default)
        self.current_file = ""
        self.codearea_header.set_value = ""
    
    def close_project(self, ev=True):
        # Save filesystem
        self.save_fs()
        # Return to project picker
        self.project_picker(ev)
    
    def path_open(self, ev=None, path=None, entrytype=None, mousebutton=1, action="open"):
        if ev: # Get variables from event
            path = ev.target["path"]
            entrytype = ev.target["entrytype"]
            mousebutton = ev.which
            action = ev.target["action"]
        # Get filename
        filename = os.path.basename(os.path.normpath(path))
        if action == "download": # Download action
            if entrytype == "fs.file": # file
                window.download(self.vfs.open(path).read(), filename) # download file
            elif entrytype == "fs.directory": # directory
                pass # TODO: download as ZIP file
        elif action == "delete": # Delete action
            if window.confirm(f'"{filename}" will be deleted PERMANENTLY! Are you sure to continue?'):
                if path == self.current_file:
                    self.close_file()
                self.vfs._remove_inode(path)
        elif action == "rename": # Rename action
            newname = window.prompt(f'What to name "{filename}"?')
            if newname and newname != "":
                self.vfs.rename(path, newname)
        elif action == "open":
            if mousebutton == 1: # primary click
                if entrytype == "fs.directory": # directory
                    # Open directory
                    self.vfs.chdir(path)
                    self.reload_filemanager(self.vfs.getcwd())
                    # Disable the one up button if current directory is /
                    if self.vfs.getcwd() == "/":
                        self.filemenu_one_up_btn.disabled = True
                    else:
                        self.filemenu_one_up_btn.disabled = False
                if entrytype == "fs.file": # file
                    # Save file if possible
                    try:
                        self.save_file()
                    except:
                        pass
                    # Set new current_path
                    self.current_file = path
                    # Write its contents into the codearea
                    data = self.vfs.open(path, mode="r").read()
                    self.codearea.set_value(data)
                    # Set codeareas header
                    self.codearea_header.set_text(path)
                    # Enable the save button and codearea
                    self.codearea_save_btn.disabled = False
                    self.codearea_run_btn.disabled = False
                    self.codearea.disabled = False
            if mousebutton == 3: # secondary click
                pass # TODO

    def filemenu_create(self, flist, header=None):
        # clear menu
        self.filemenu.clear()
        # Append header if defined
        if header:
            # The header itself
            self.filemenu_append(B(header))
            # An empty space
            self.filemenu_append(P("⠀", style={"font-size": 0, "word-wrap": "break-word"}))
        # If flist is empty; create default entry
        if flist == []:
            self.filemenu_append(TR(TH("Directory is empty")))
        # append entries from flist
        for entry in flist:
            # Create elements
            elem_open = TH(entry[0]["name"], action="open", entrytype=entry[0]["type"], path=entry[0]["path"])
            elem_rename = TH(theme.get_icon("edit"), action="rename", entrytype=entry[0]["type"], path=entry[0]["path"], width="5%")
            elem_delete = TH(theme.get_icon("delete"), action="delete", entrytype=entry[0]["type"], path=entry[0]["path"], width="5%")
            elem_download = TH(theme.get_icon("download"), action="download", entrytype=entry[0]["type"], path=entry[0]["path"], width="5%")
            # Set class manually (workaround)
            elem_open["class"] = "ui_cell"
            elem_rename["class"] = "ui_cell"
            elem_delete["class"] = "ui_cell"
            elem_download["class"] = "ui_cell"
            # Create TR element
            element = TR(style={"cursor": "pointer", "user-select": "none"})
            # Append TH elements
            element.appendChild(elem_open)
            element.appendChild(elem_rename)
            element.appendChild(elem_delete)
            element.appendChild(elem_download)
            # Set action
            if not entry[1]: entry[1] = None
            self.filemenu_append(element, action=entry[1])
    
    def filemenu_append(self, entry, action=None):
        child = self.filemenu.appendChild(entry)
        # Bind actions to element
        if action: 
            child.bind("mouseup", action) # Normal mouseup action
        return child
    
    def reload_filemanager(self, basepath=None):
        vfs = self.vfs
        files = []
        # Get basepath if not supplied as an argument
        if not basepath:
            basepath = vfs.getcwd()
        # Get list of files
        for filename in vfs.listdir(basepath):
            # Get path
            path = vfs.realpath(filename)
            # Check weather it's a file or a directory
            if vfs.isfile(path):
                # Set prefix
                prefix = "-"
                # Add menu entry
                files.append([{"name": f"[{prefix}] {filename}", "type": "fs.file", "path": path}, self.path_open])
            elif vfs.isdir(path):
                # Set prefix
                prefix = ">"
                # Add menu entry
                files.append([{"name": f"[{prefix}] {filename}", "type": "fs.directory", "path": path}, self.path_open])
        # Create menu
        self.filemenu_create(flist=files, header=basepath)
    
    def update_path(self, when, path):
        if not self._enable_update_event:
            return
        if when == 1:
            # Remove trailing /
            if path[-1] == "/" and not len(path) == 1:
                path = path[:-1]
            # Check weather the change affects the filemanager or code editor
            if path.startswith(self.vfs.getcwd()):
                self.reload_filemanager() # Reload directory
            elif path == self.current_file:
                self.path_open(path=path, entrytype="fs.file", mousebutton=1) # Reload file
