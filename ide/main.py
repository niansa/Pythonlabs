# Load basic stuff
import os
os.chdir("/ui")
from browser import document, window
from projectpicker import projectpicker_class
import storage, screens

# Initalise local storage
storage.init()

# Create project load function
def project_load(project_fs, project_name, noeventinit=False):
    # Show loading screen and hide projectpicker screen
    screens.switch("loading", "Loading IDE...")
    from ui import ui_class
    from virtualfs import vfs_class
    # Initalise virtual filesystem
    vfs = vfs_class(project_fs)
    # Let it take over the filesystem
    vfs._takeover_fs()
    # Initalise the UI
    ui = ui_class(vfs, project_picker, project_name, noeventinit)
    # Load file menu
    ui.reload_filemanager()
    # Show ui screen and hide loading screen
    screens.switch("ui")

def project_picker(ev=None):
    # hide ui screen and show loading screen
    screens.switch("loading", "Loading projects...")
    # Check weather to bind events or not
    if ev:
        noeventinit = True
    else:
        noeventinit = False
    # Initalise project picker
    projectpicker = projectpicker_class(project_load, noeventinit)
    projectpicker.load_projects()
    # Hide loading screen and show projectpicker screen
    screens.switch("projectpicker")

project_picker()
